const todosData = require("./todosData")

class Controller {

    // Getting all todos
    async getAllTodos() {
        return new Promise((resolve,reject) => {
            (todosData.length === 0) ? reject("The todos data is empty") : resolve(todosData)
        })
    }

    //Getting a single todo

    async getTodo(id) {
        return new Promise((resolve,reject) => {
            const todo = todosData.find(todo=>todo.id === parseInt(id))
            if (todo) {
                resolve(todo)
            } else {
                reject(`Todo with id ${id} is not found`)
            }
        })
    }

    //  Creating a todo 
    async createNewTodo(todo) {
        return new Promise((resolve,reject) => {
            const newTodo = {
                id : Math.floor(30 + Math.random()*100),
                ...todo
            }
            todosData.push(newTodo)
            resolve(newTodo)
        })
    }

    //Updating a todo

    async updateTodo(id) {
        return new Promise((resolve,reject)=>{
            const todo = todosData.find(todo=>todo.id===parseInt(id))
            if (!todo) {
                reject(`No todo with id ${id} found`)
            } else {
                todo["completed"] = true
                resolve(todo)
            }
        })
    }

    //Deleting a todo

    async deleteTodo(id) {
        return new Promise((resolve,reject)=>{
            const todo = todosData.find(todo=>todo.id===parseInt(id))
            if (!todo) {
                reject(`No todo with id ${id} found`) 
            } else {
                resolve(`todo deleted successfully`)
            }
        })
    }

}

module.exports = Controller
