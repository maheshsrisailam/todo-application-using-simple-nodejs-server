const http = require("http")
const Todo = require("./controller")
const getRequestData = require("./requestData")

const server = http.createServer(async (request, response) => {
    if (request.url === "/todos" && request.method ==="GET") {
        try {
            const todos = await new Todo().getAllTodos()
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todos))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message:error}))
        }
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="GET") {
        try {
            const id = request.url.split('/')[2];
            const todo = await new Todo().getTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message:error}))
        }
    } else if (request.url==='/todos' && request.method ==="POST") {
        try {
            const todoData = await getRequestData(request)
            const todo = await new Todo().createNewTodo(JSON.parse(todoData))
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error}))
        }
        //request.url.match(/\/todos\/([0-9]+)/) 
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="PATCH") {
        try {
            const id = request.url.split('/')[2];
            const updatedTodo = await new Todo().updateTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(updatedTodo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
        
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="PUT") {
        try {
            const id = request.url.split('/')[2];
            const updatedTodo = await new Todo().updateTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(updatedTodo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
        
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="DELETE") {
        try {
            const id = request.url.split('/')[2];
            const message = await new Todo().deleteTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(message))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
    } else {
        response.writeHead(404, {"Content-Type": "application/json"})
        response.end(JSON.stringify({message: "URL is not matched"}))
    }
})

server.listen(3000, () => console.log("Server is started listening on port 3000"));
