const getRequestData = (request) => {
    return new Promise((resolve,reject)=>{
        try{
            let body = ""
            request.on("data",data => {
                
                body += data.toString()

            })

            request.on("end", () => {
                resolve(body)
            })
        } catch (error){
            reject(error)
        }
    })
}

module.exports = getRequestData